#ifndef __DEFINES__
#define __DEFINES__
#include "vector"

#define VECTOR1 std::vector<double long>
#define VECTOR2 std::vector<std::vector<double long>>
#define VECTOR3 std::vector<std::vector<std::vector<double long>>>
#define VECTOR4 std::vector<std::vector<std::vector<std::vector<double long>>>>
#define VECTOR5 std::vector<std::vector<std::vector<std::vector<std::vector<double long>>>>>

#define INTVECTOR1 std::vector<int>
#define INTVECTOR2 std::vector<std::vector<int>>
#define INTVECTOR3 std::vector<std::vector<std::vector<int>>>

#endif