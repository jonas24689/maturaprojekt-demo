# ifndef __IMAGEMANAGEMENT__
# define __IMAGEMANAGEMENT__
#include "defines.h"
#include <string>

class imageManagement
{
private:
    std::string TEXTIMAGESPATH = "../../../textimagesnormNN/";
    std::vector<std::string> loadImageList();
    void loadGoodImagesIndexes(int trainSetSize, INTVECTOR1* trainSet, INTVECTOR1* testSet);
    VECTOR4 loadBoundaryBoxes();

public:
    VECTOR2 loadOneImage(std::string imgName);
    std::vector<std::string> ImagesNamesList;
    INTVECTOR1 trainingIndexes;
    INTVECTOR1 testingIndexes;
    VECTOR4 boundaryBoxesAllImages;

    void shuffleImageIndex();

    imageManagement();
    ~imageManagement();
};




#endif