#include "Cnn.h"
#include "helperfunctions.h"

//momentum
void Cnn::sgdMomentum(double long *velocity, double long *toUpdate ,double long update){
    *velocity = momentumGamma * *velocity + lrRate * update;
    *toUpdate = *toUpdate - *velocity;
}

void Cnn::initMomentum(){
    velocitysbiases = helperfunctions::zeroVector(biases);
    velocitysweights = helperfunctions::zeroVector(weights);
    velocitysfilters = helperfunctions::zeroVector(filters);
}

//plain SGD
void Cnn::stochasticGradientDescent(double long *toUpdate, double long update){
    *toUpdate += -lrRate * update;
}