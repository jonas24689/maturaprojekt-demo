#ifndef __HELPERFUNCTIONS__
#define __HELPERFUNCTIONS__
#include <vector>
#include <iostream>
#include <string>
#include <iomanip>
#include "math.h"
#define VEC1TYPE std::vector<Type>
#define VEC2TYPE std::vector<std::vector<Type>>
#define VEC3TYPE std::vector<std::vector<std::vector<Type>>>
#define VEC4TYPE std::vector<std::vector<std::vector<std::vector<Type>>>>

namespace helperfunctions{

    template <typename Type>
    void print(Type toPrint){
        std::cout << toPrint << std::fixed <<  std::endl;
    }
    
    template <typename Type> 
    void printVector(VEC1TYPE vector){
        std::cout << "{";
        for(int x = 0; x<vector.size(); x++){
            std::cout << std::setprecision(10)<< vector[x] << std::fixed;
            
            if(x != vector.size()-1)
            std::cout << ", ";
        }
        std::cout << "}";
    }

    template <typename Type> 
    void printVector(VEC2TYPE vector){
        std::cout << "{";
        for(int x = 0; x<vector.size(); x++){
            printVector(vector[x]);
            if(x != vector.size()-1)
            std::cout <<", " << std::endl;
        }
        std::cout << "}" << std::endl;
    }

    template <typename Type> 
    void printVector(VEC3TYPE vector){
        std::cout << "{";
        for(int x = 0; x<vector.size(); x++){
            printVector(vector[x]);
            if(x != vector.size()-1)
            std::cout << ", ";
        }
        std::cout << "}" << std::endl;
    }

    template <typename Type> 
    void printVector(VEC4TYPE vector){
        std::cout << "{";
        for(int x = 0; x<vector.size(); x++){
            printVector(vector[x]);
            if(x != vector.size()-1)
            std::cout << ", ";
        }
        std::cout << "}" << std::endl;
    }

    template <typename Type>
    VEC1TYPE flattenVector(VEC2TYPE vector){
        VEC1TYPE tempVector = {};
        for (int i = 0; i < vector.size(); i++){
            tempVector.insert(tempVector.end(), vector[i].begin(), vector[i].end());
        }
        return tempVector;
    }

    template <typename Type>
    VEC1TYPE flattenVector(VEC3TYPE vector){
        VEC1TYPE tempVector = {};
        for (int i = 0; i < vector.size(); i++){
            VEC1TYPE vec = flattenVector(vector[i]);
            tempVector.insert(tempVector.end(), vec.begin(), vec.end());
        }
        return tempVector;
    }

    template <typename Type>
    VEC1TYPE flattenVector(VEC4TYPE vector){
        VEC1TYPE tempVector = {};
        for (int i = 0; i < vector.size(); i++){
            VEC1TYPE vec = flattenVector(vector[i]);
            tempVector.insert(tempVector.end(), vec.begin(), vec.end());
        }
        return tempVector;
    }

    template <typename Type>
    VEC1TYPE zeroVector(VEC1TYPE vec){
        return VEC1TYPE(vec.size());
    }

    template <typename Type>
    VEC2TYPE zeroVector(VEC2TYPE vec){
        VEC2TYPE zVec = {};
        for(int i = 0; i<vec.size(); i++) zVec.push_back(zeroVector(vec[i]));
        return zVec;
    }


    template <typename Type>
    VEC3TYPE zeroVector(VEC3TYPE vec){
        VEC3TYPE zVec = {};
        for(int i = 0; i<vec.size(); i++) zVec.push_back(zeroVector(vec[i]));
        return zVec;
    }

    template <typename Type>
    VEC4TYPE zeroVector(VEC4TYPE vec){
        VEC4TYPE zVec = {};
        for(int i = 0; i<vec.size(); i++) zVec.push_back(zeroVector(vec[i]));
        return zVec;
    }

    template <typename Type>
    VEC3TYPE shapeVector(VEC1TYPE flattendVector, VEC3TYPE shapeVector){
        VEC3TYPE shapedVector = helperfunctions::zeroVector(shapeVector);
        int index = 0;

        for (int x = 0; x < shapeVector.size(); x++){
            for (int y = 0; y < shapedVector[x].size(); y++){
                for(int z = 0; z < shapedVector[x][y].size(); z++){
                    shapedVector[x][y][z] = flattendVector[index++];
                }
            }
            
        }
        return shapedVector;
    }

    long double sumVector(std::vector<double long> vec);
    
    //form => [[[a]], [[b],[c]]] => [[a]], [[b],[c]] 
    std::vector<std::string> splitStringToVector(std::string stringtosplit);
    
    std::string removeSpaces(std::string string);

    //form "23", "42", "3452"
    std::vector<long double> StringVectorToNumberVector(std::vector<std::string> stringVector);

    //form => [[[][]], [], []]]
    std::vector<std::vector<std::vector<double long>>> DataFromPerLineString(std::string line);
}
#endif