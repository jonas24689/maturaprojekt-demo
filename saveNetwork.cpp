#include "saveNetwork.h"
#include <iostream>
#include <iomanip>

saveNetwork::saveNetwork(std::string filePath){
    file = filePath;
}

saveNetwork::~saveNetwork(){

}

void saveNetwork::saveVEC1(VECTOR1 vec1){
    networkFile << vec1.size() << std::endl;
    for(auto a : vec1){
        networkFile << std::setprecision(PRECISION) << a << std::endl;
    }
}

void saveNetwork::saveVEC1(INTVECTOR1 vec1){
    networkFile << vec1.size() << std::endl;
    for(auto a : vec1){
        networkFile << a << std::endl;
    }
}

void saveNetwork::saveVEC2(VECTOR2 vec2){
    networkFile << vec2.size() << std::endl;
    for(VECTOR1 a : vec2){
        saveVEC1(a);
    }
}

void saveNetwork::saveVEC2(INTVECTOR2 vec2){
    networkFile << vec2.size() << std::endl;
    for(INTVECTOR1 a : vec2){
        saveVEC1(a);
    }
}

void saveNetwork::saveVEC3(VECTOR3 vec3){
    networkFile << vec3.size() << std::endl;
    for(VECTOR2 a : vec3){
        saveVEC2(a);
    } 
}

void saveNetwork::saveVEC3(INTVECTOR3 vec3){
    networkFile << vec3.size() << std::endl;
    for(INTVECTOR2 a : vec3){
        saveVEC2(a);
    }
    
}

void saveNetwork::saveVEC4(VECTOR4 vec4){
    networkFile << vec4.size() << std::endl;
    for(VECTOR3 a : vec4){
        saveVEC3(a);
    }
    
}

void saveNetwork::saveDNN(INTVECTOR1 NNsize, VECTOR2 biases, VECTOR3 weights){
    //save NNsize
    saveVEC1(NNsize);
    //save biases
    saveVEC2(biases);
    //save weights
    saveVEC3(weights);
}

void saveNetwork::saveCNN(VECTOR4 filters, VECTOR2 maxPool, INTVECTOR3 filtersSize){
    //save maxPool
    saveVEC2(maxPool);
    //save filters
    saveVEC4(filters);

    //save filtersSize
    saveVEC3(filtersSize);
}

void saveNetwork::saveVelocitysMomentum(VECTOR2 biasesMomentum, VECTOR3 weightsMomentum, VECTOR4 filtersMomentum){
    saveVEC2(biasesMomentum);
    saveVEC3(weightsMomentum);
    saveVEC4(filtersMomentum);
}

void saveNetwork::saveWholeNetwork(INTVECTOR1 NNsize, VECTOR2 biases, VECTOR3 weights, VECTOR4 filters, VECTOR2 maxPool, INTVECTOR3 filtersSize){
    networkFile.open(file);
    saveDNN(NNsize, biases, weights);
    saveCNN(filters, maxPool, filtersSize);
    networkFile.close();
}

void saveNetwork::saveWholeNetworkVelocitys(INTVECTOR1 NNsize, VECTOR2 biases, VECTOR3 weights, VECTOR4 filters, VECTOR2 maxPool, INTVECTOR3 filtersSize, VECTOR2 biasesMomentum, VECTOR3 weightsMomentum, VECTOR4 filtersMomentum){
    networkFile.open(file);
    saveDNN(NNsize, biases, weights);
    saveCNN(filters, maxPool, filtersSize);
    saveVelocitysMomentum(biasesMomentum, weightsMomentum, filtersMomentum);
    networkFile.close();
}

VECTOR1 saveNetwork::readVEC1(){
    int loop;
    VECTOR1 readVec = {};
    networkFile >> loop;
    for (int i = 0; i < loop; i++){
        double long nmbr;
        networkFile >> std::setprecision(PRECISION) >> nmbr;
        readVec.push_back(nmbr);
    }
    return readVec;
}

INTVECTOR1 saveNetwork::readVEC1Int(){
    int loop;
    INTVECTOR1 readVec = {};
    networkFile >> loop;
    for (int i = 0; i < loop; i++){
        int nmbr;
        networkFile >> nmbr;
        readVec.push_back(nmbr);
    }
    return readVec;
}

VECTOR2 saveNetwork::readVEC2(){
    int loop;
    VECTOR2 readVec = {};
    networkFile >> loop;
    for (int i = 0; i < loop; i++){
        readVec.push_back(readVEC1());
    }
    return readVec;
}

INTVECTOR2 saveNetwork::readVEC2Int(){
    int loop;
    INTVECTOR2 readVec = {};
    networkFile >> loop;
    for (int i = 0; i < loop; i++){
        readVec.push_back(readVEC1Int());
    }
    return readVec;
}

VECTOR3 saveNetwork::readVEC3(){
    int loop;
    VECTOR3 readVec = {};
    networkFile >> loop;
    for (int i = 0; i < loop; i++){
        readVec.push_back(readVEC2());
    }
    return readVec;
}

INTVECTOR3 saveNetwork::readVEC3Int(){
    int loop;
    INTVECTOR3 readVec = {};
    networkFile >> loop;
    for (int i = 0; i < loop; i++){
        readVec.push_back(readVEC2Int());
    }
    return readVec;
}

VECTOR4 saveNetwork::readVEC4(){
    int loop;
    VECTOR4 readVec = {};
    networkFile >> loop;
    for (int i = 0; i < loop; i++){
        readVec.push_back(readVEC3());
    }
    return readVec;
}

void saveNetwork::readWholeNetwork(INTVECTOR1* NNsize, VECTOR2* biases, VECTOR3* weights, VECTOR4* filters, VECTOR2* maxPool, INTVECTOR3* filtersSize){
    networkFile.open(file);
    *NNsize=readVEC1Int();
    *biases=readVEC2();
    *weights=readVEC3();
    
    *maxPool=readVEC2();
    *filters=readVEC4();
    *filtersSize = readVEC3Int();
    networkFile.close();
}

void saveNetwork::readConstructorProps(INTVECTOR1* NNsize, INTVECTOR3* filtersSize, VECTOR2* maxPool){
    networkFile.open(file);
    *NNsize=readVEC1Int();
    readVEC2();
    readVEC3();
    
    *maxPool=readVEC2();
    readVEC4();
    *filtersSize = readVEC3Int();
    networkFile.close();
}

void saveNetwork::changeFile(std::string filePath){
    file = filePath;
}