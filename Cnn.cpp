#include "Cnn.h"
#include "helperfunctions.h"
#include <cmath>
#include <iostream>
#include <fstream>

Cnn::Cnn(){
    imManager = new imageManagement();
    helperfunctions::print(imManager->trainingIndexes.size());
    helperfunctions::print(imManager->testingIndexes.size());
    filters=createRandomFilters( {INTVECTOR2(1, {3,3}), INTVECTOR2(2, {6, 6}), INTVECTOR2(3, {7, 8}), INTVECTOR2(2, {5,5})} );
    maxPools=VECTOR2(filters.size(), VECTOR1());
    //maxPools[0] = {3, 3};
    maxPools[2] = {2, 2};
    
    NNsize = {74556, 30, 20, 20, 4};
    weights = createRandomWeights();
    biases = createRandomBiases();
    as = createZeroZsAndAs();
    zs = createZeroZsAndAs();

    initMomentum();
}

Cnn::Cnn(INTVECTOR1 NNsizeP, INTVECTOR3 filtersSize, VECTOR2 maxPoolsP){
    imManager = new imageManagement();
    helperfunctions::print(imManager->trainingIndexes.size());
    helperfunctions::print(imManager->testingIndexes.size());

    filters=createRandomFilters(filtersSize);
    NNsize = NNsizeP;
    maxPools = maxPoolsP;

    weights = createRandomWeights();
    biases = createRandomBiases();
    as = createZeroZsAndAs();
    zs = createZeroZsAndAs();

    initMomentum();
}

Cnn::Cnn(INTVECTOR1 NNsizeP, VECTOR2 biasesP, VECTOR3 weightsP, VECTOR4 filtersP ,INTVECTOR3 filtersSizeP, VECTOR2 maxPoolsP){
    imManager = new imageManagement();
    helperfunctions::print(imManager->trainingIndexes.size());
    helperfunctions::print(imManager->testingIndexes.size());

    filtersSize = filtersSizeP;
    filters=filtersP;
    NNsize = NNsizeP;
    maxPools = maxPoolsP;

    weights = weightsP;
    biases = biasesP;
    as = createZeroZsAndAs();
    zs = createZeroZsAndAs();

    initMomentum();
}

VECTOR2 Cnn::createRandomBiases(){
    VECTOR2 tempBiases;
    //emptyForInputLayer
    tempBiases.push_back({});
    for (int layer = 1; layer < NNsize.size(); layer++){
        VECTOR1 biasesForOneLayer;
        for (int neuronBias = 0; neuronBias < NNsize[layer]; neuronBias++){
                biasesForOneLayer.push_back(double(rand() % 100000 + 1) / 1000000);
        }
        tempBiases.push_back(biasesForOneLayer);
    }
    return tempBiases;
}

VECTOR2 Cnn::createZeroZsAndAs(){
    VECTOR2 tempZA;
    //emptyForInputLayer
    tempZA.push_back({});
    for (int layer = 1; layer < NNsize.size(); layer++){
        VECTOR1 ZAforOneLayer;
        for (int neuronZA = 0; neuronZA < NNsize[layer]; neuronZA++){
                ZAforOneLayer.push_back(0);
        }
        tempZA.push_back(ZAforOneLayer);
    }
    return tempZA;
}

VECTOR3 Cnn::createRandomWeights(){
    VECTOR3 tempWeights;
    for(int layer = 0; layer<NNsize.size()-1; layer++){
        VECTOR2 oneLayerWeights;
        for (int leftNeuron = 0; leftNeuron < NNsize[layer]; leftNeuron++){
            VECTOR1 oneLeftNeuronWeights;
            for (int rightNeuron = 0; rightNeuron < NNsize[layer+1]; rightNeuron++){
                oneLeftNeuronWeights.push_back(double(rand() % 100000 + 1) / 1000000);
            }
            oneLayerWeights.push_back(oneLeftNeuronWeights);
        }
        tempWeights.push_back(oneLayerWeights);
    }
    return tempWeights;
}

//filter must be rectangular
VECTOR2 Cnn::useFilter(VECTOR2 image, VECTOR2 filter, VECTOR2* noActivationFunc){
    int filtersizeX = filter.size();
    int filtersizeY = filter[0].size();
    VECTOR2 filterd(image.size()-(filtersizeX-1), VECTOR1(image[0].size()-(filtersizeY - 1)));
    VECTOR2 filterednoActivationfunctionTemp(image.size()-(filtersizeX-1), VECTOR1(image[0].size()-(filtersizeY - 1)));
    for(int x = 0; x<image.size()-(filtersizeX - 1); x++){
        for(int y = 0; y<image[x].size()-(filtersizeY-1); y++){
            
            for(int xfilter = 0; xfilter<filtersizeX; xfilter++){
                for(int yfilter = 0; yfilter<filtersizeY; yfilter++){
                    filterd[x][y] += image[xfilter+x][yfilter+y]*filter[xfilter][yfilter];
                }
            }
            //use activationFuncion on Filtered
            filterednoActivationfunctionTemp[x][y] = filterd[x][y];
            filterd[x][y] = filter_Activation(filterd[x][y]);
        }
    }
    *noActivationFunc = filterednoActivationfunctionTemp;
    return filterd;
}

VECTOR2 Cnn::maxPool(VECTOR2 image, int sizeX, int sizeY, VECTOR2* imagePoolingMap){
    VECTOR2 pooled(image.size()/sizeX, VECTOR1(image[0].size()/sizeY));
    VECTOR2 tempImagePooling = helperfunctions::zeroVector(image);

    for(int x = 0; x<image.size()/sizeX; x++){
        for(int y = 0; y<image[x].size()/sizeY; y++){
            
            double long max = -__DBL_MAX__;
            int maxXindex, maxYindex;
            for(int xPool = 0; xPool<sizeX; xPool++){
                for(int yPool = 0; yPool<sizeY; yPool++){
                    if(max < image[x*sizeX + xPool][y*sizeY + yPool]){
                        max=image[x*sizeX + xPool][y*sizeY + yPool];
                        maxXindex = x*sizeX + xPool;
                        maxYindex = y*sizeY + yPool;
                    } 
                }
            }
            pooled[x][y] = max;
            tempImagePooling[maxXindex][maxYindex] = 1;
        }
    }
    *imagePoolingMap = tempImagePooling;

    return pooled;
}

VECTOR4 Cnn::createRandomFilters(INTVECTOR3 filterstruct){
    VECTOR4 allRandomFilters;
    for(int layer = 0; layer< filterstruct.size(); layer++){
        VECTOR3 filtersForOneLayer;
        for(int filterIndex = 0; filterIndex<filterstruct[layer].size();filterIndex++){
            VECTOR2 OneFilter;
            for (int x = 0; x < filterstruct[layer][filterIndex][0]; x++){
                VECTOR1 filterys;
                for (int y = 0; y < filterstruct[layer][filterIndex][1]; y++){
                    filterys.push_back(double(rand() % 100000 + 1) / 1000000);
                }
                OneFilter.push_back(filterys);
            }
            filtersForOneLayer.push_back(OneFilter);
        }
        allRandomFilters.push_back(filtersForOneLayer);
    }
    return allRandomFilters;
}

VECTOR3 Cnn::feedForwardFilters(VECTOR2 image){
    VECTOR3 filteredImage = {image};
    processingImagesNoActivationFunction.clear();
    processingImagesMaxPooled.clear();
    processingImagesMaxPooled.push_back(filteredImage);
    processingImagesNoActivationFunction.push_back(filteredImage);
    //set to empty Vector with offset ==> for Layer
    maxPoolingMap = {{}};

    for (int filterLayer = 0; filterLayer < filters.size(); filterLayer++){
        VECTOR3 tempFilteredMaxPooled;
        VECTOR3 tempjustFiltered;
        VECTOR3 oneLayerMaxPoolMapping;
        for (int d2imageIndex = 0; d2imageIndex < filteredImage.size(); d2imageIndex++){
            for (int d3Layer = 0; d3Layer < filters[filterLayer].size(); d3Layer++){

                VECTOR2 imgNoActivationFunction;
                VECTOR2 d2tempImage = useFilter(filteredImage[d2imageIndex], filters[filterLayer][d3Layer], &imgNoActivationFunction);
                tempjustFiltered.push_back(imgNoActivationFunction);

                VECTOR2 oneImageMaxPoolMapping = {};

                if(maxPools[filterLayer].size() == 2){
                    d2tempImage = maxPool(d2tempImage, maxPools[filterLayer][0], maxPools[filterLayer][1], &oneImageMaxPoolMapping);
                }
                else{
                    oneImageMaxPoolMapping = VECTOR2(d2tempImage.size(), VECTOR1(d2tempImage[0].size(),1));
                }
                tempFilteredMaxPooled.push_back(d2tempImage);
                oneLayerMaxPoolMapping.push_back(oneImageMaxPoolMapping);
            }
            
        }
        processingImagesNoActivationFunction.push_back(tempjustFiltered);
        filteredImage = tempFilteredMaxPooled;
        processingImagesMaxPooled.push_back(filteredImage);
        maxPoolingMap.push_back(oneLayerMaxPoolMapping);
    }
    return filteredImage;
}

VECTOR2 Cnn::maxPoolCosWRaMapping(VECTOR2 maxPoolmap, int layer, VECTOR2 cosWRa){
    int sizeX = maxPools[layer][0];
    int sizeY = maxPools[layer][1];
    VECTOR2 cosWRarealMap = helperfunctions::zeroVector(maxPoolmap);
    for(int x = 0; x<maxPoolmap.size()/sizeX; x++){
        for(int y = 0; y<maxPoolmap[x].size()/sizeY; y++){
            
            for(int xPool = 0; xPool<sizeX; xPool++){
                for(int yPool = 0; yPool<sizeY; yPool++){
                    cosWRarealMap[x*sizeX + xPool][y*sizeY + yPool] = cosWRa[x][y] * maxPoolmap[x*sizeX + xPool][y*sizeY + yPool];
                }
            }
        }
    }
    return cosWRarealMap;
}

void Cnn::sgdFilters(VECTOR3 cosWRa, VECTOR4* filterUpdates){
    VECTOR4 tempfilterUpdates = helperfunctions::zeroVector(filters);
    int plindex = processingImagesMaxPooled.size() -1;
    for (int layer = plindex; layer > 0; layer--){
        VECTOR3 cosWRnextA = {};
        for (int filter = 0; filter < filters[layer-1].size(); filter++){
            
            for (int imgBeforePerFilter = 0; imgBeforePerFilter < processingImagesMaxPooled[layer-1].size(); imgBeforePerFilter++){
                VECTOR2 oneImageCosWRnextA = helperfunctions::zeroVector(processingImagesMaxPooled[layer-1][imgBeforePerFilter]);
                VECTOR2 fCosWRa;
                if(maxPools[layer-1].size() == 2)
                    fCosWRa = maxPoolCosWRaMapping(maxPoolingMap[layer][imgBeforePerFilter], layer-1, cosWRa[imgBeforePerFilter]);
                else
                    fCosWRa = cosWRa[imgBeforePerFilter];
                
                for (int imgX = 0; imgX < processingImagesMaxPooled[layer-1][imgBeforePerFilter].size()-(filters[layer-1][filter].size()-1); imgX++){
                    for (int imgY = 0; imgY < processingImagesMaxPooled[layer-1][imgBeforePerFilter][imgX].size()-(filters[layer-1][filter][0].size()-1); imgY++){
                        
                        double long acitvationfunction_der = DER_filter_Activation(processingImagesNoActivationFunction[layer][filter*processingImagesMaxPooled[layer-1].size()+imgBeforePerFilter][imgX][imgY]);

                        for (int filterX = 0; filterX < filters[layer-1][filter].size(); filterX++){
                            for (int filterY = 0; filterY < filters[layer-1][filter][filterX].size(); filterY++){

                                tempfilterUpdates[layer-1][filter][filterX][filterY] += fCosWRa[imgX][imgY] * acitvationfunction_der * processingImagesMaxPooled[layer-1][imgBeforePerFilter][imgX+filterX][imgY+filterY];
                                oneImageCosWRnextA[imgX+filterX][imgY+filterY] += fCosWRa[imgX][imgY] * acitvationfunction_der * filters[layer-1][filter][filterX][filterY];
                            }
                        }
                    }

                }
                cosWRnextA.push_back(oneImageCosWRnextA);
            }
            
        }
        cosWRa = cosWRnextA;
    }
    *filterUpdates = tempfilterUpdates;
}


VECTOR1 Cnn::feedForwardDNN(VECTOR1 input){
    VECTOR1 inputForLayer = input;
    as[0]=input;
    for(int layer = 1; layer<NNsize.size(); layer++){
        VECTOR1 newas;
        VECTOR1 newzs;
        for (int rightNeuron = 0; rightNeuron < NNsize[layer]; rightNeuron++){
            double long z = 0;
            for (int leftNeuron = 0; leftNeuron < NNsize[layer-1]; leftNeuron++){
                z += weights[layer-1][leftNeuron][rightNeuron] * inputForLayer[leftNeuron];
            }
            z += biases[layer][rightNeuron];
            newzs.push_back(z);
            newas.push_back(leaky_RELU(z));
        }
        inputForLayer = newas;
        zs[layer] = newzs;
        as[layer] = newas;
    }
    return inputForLayer;
}

VECTOR1 Cnn::sgdDNN(VECTOR1 desiredOutput, VECTOR2* biasesUpdate, VECTOR3* weightsUpdate){
    int Nlindex = NNsize.size() -1;
    VECTOR1 cosWRa = {};
    VECTOR2 tempBiasesUpdate = helperfunctions::zeroVector(biases);
    VECTOR3 tempWeightsUpdate = helperfunctions::zeroVector(weights);

    for(int lastneurons = 0; lastneurons < NNsize[Nlindex]; lastneurons++){
        cosWRa.push_back(DER_cos(as.back()[lastneurons], desiredOutput[lastneurons]));
    }

    for (int layer = Nlindex; layer > 0; layer--){
        VECTOR1 cosWRnextA = VECTOR1(NNsize[layer-1]);

        for(int neuronRight = 0; neuronRight < NNsize[layer]; neuronRight++){
            double long cosWRz = cosWRa[neuronRight] * DER_leaky_RELU(zs[layer][neuronRight]);
            tempBiasesUpdate[layer][neuronRight] = cosWRz;

            for(int neuronLeft = 0; neuronLeft < NNsize[layer-1]; neuronLeft++){   
                tempWeightsUpdate[layer-1][neuronLeft][neuronRight] = cosWRz * as[layer-1][neuronLeft];
                cosWRnextA[neuronLeft] += cosWRz * weights[layer-1][neuronLeft][neuronRight];
            }

        }
        cosWRa = cosWRnextA;
    }
    *biasesUpdate = tempBiasesUpdate;
    *weightsUpdate = tempWeightsUpdate;
    return cosWRa;
    
}

VECTOR1 Cnn::feedForwardWholeNN(VECTOR2 imageMatrix){
    VECTOR3  filteredImages = feedForwardFilters(imageMatrix);
    VECTOR1 NNinput = helperfunctions::flattenVector(filteredImages);
    VECTOR1 output = feedForwardDNN(NNinput);
    return output;
}

void Cnn::optimize(VECTOR3 weightsUpdate, VECTOR2 biasesUpdate, VECTOR4 filtersUpdate){
    for (int weightLayer = 0; weightLayer < weights.size(); weightLayer++){
        for (int neuronRight = 0; neuronRight < NNsize[weightLayer+1]; neuronRight++){
            sgdMomentum(&velocitysbiases[weightLayer+1][neuronRight], &biases[weightLayer+1][neuronRight], biasesUpdate[weightLayer+1][neuronRight]);
            //biases[weightLayer+1][neuronRight] += -lrRate * biasesUpdate[weightLayer+1][neuronRight];
            for (int neuronLeft = 0; neuronLeft < NNsize[weightLayer]; neuronLeft++){
                sgdMomentum(&velocitysweights[weightLayer][neuronLeft][neuronRight], &weights[weightLayer][neuronLeft][neuronRight], weightsUpdate[weightLayer][neuronLeft][neuronRight]);
                //weights[weightLayer][neuronLeft][neuronRight] += -lrRate * weightsUpdate[weightLayer][neuronLeft][neuronRight];
            }
            
        }
        
    }

    for (int layer = 0; layer < filters.size(); layer++){
        for (int filter = 0; filter < filters[layer].size(); filter++){
            for (int filterX = 0; filterX < filters[layer][filter].size(); filterX++){
                for (int filterY = 0; filterY < filters[layer][filter][filterX].size(); filterY++){
                    sgdMomentum(&velocitysfilters[layer][filter][filterX][filterY], &filters[layer][filter][filterX][filterY], filtersUpdate[layer][filter][filterX][filterY]);
                    //filters[layer][filter][filterX][filterY] += -lrRate * filtersUpdate[layer][filter][filterX][filterY];
                }
                
            }
            
        }
        
    }
    
    
}

void Cnn::train(int shift, int count, bool show){
    for(int index = 0; index < imManager->trainingIndexes.size()-shift && index < count; index++){
        std::cout << "Img: " << index+1 << "/" << count << " name: " << imManager->ImagesNamesList[imManager->trainingIndexes[index+shift]] << std::endl;
        VECTOR2 imageMatrix = imManager->loadOneImage(imManager->ImagesNamesList[imManager->trainingIndexes[index+shift]]);
        VECTOR1 desiredoutput = helperfunctions::flattenVector(imManager->boundaryBoxesAllImages[imManager->trainingIndexes[index+shift]][0]);
        //output is the same as as.back()
        VECTOR1 output = feedForwardWholeNN(imageMatrix);
        VECTOR2 biasesUpdate = {};
        VECTOR3 weightsUpdate = {};
        VECTOR1 cosWRa = sgdDNN(desiredoutput, &biasesUpdate, &weightsUpdate);
        VECTOR3 shapedCosWRa = helperfunctions::shapeVector(cosWRa, processingImagesMaxPooled.back());
        VECTOR4 filterUpdates = {};
        sgdFilters(shapedCosWRa, &filterUpdates);
        optimize(weightsUpdate, biasesUpdate, filterUpdates);
        helperfunctions::print("Output: ");
        helperfunctions::printVector(output);
        std::cout << std::endl << "desiredOutput: " << std::endl;
        helperfunctions::printVector(desiredoutput);
        helperfunctions::print("Losses: ");
        for(int index = 0; index < desiredoutput.size(); index++){
            helperfunctions::print(Cnn::cos(output[index], desiredoutput[index]));
        }
        if(show){
            std::string imgName = imManager->ImagesNamesList[imManager->trainingIndexes[index+shift]];
            showImage(output, desiredoutput ,imgName.substr(0, imgName.size()-3)+"jpg");
        }
    }
}

VECTOR1 Cnn::predictSet(int shift , int count, bool show){
    for(int index = 0; index < imManager->testingIndexes.size()-shift && index < count; index++){
        std::cout << "Img: " << index+1 << "/" << count << " name: " << imManager->ImagesNamesList[imManager->testingIndexes[index+shift]] << std::endl;
        VECTOR2 imageMatrix = imManager->loadOneImage(imManager->ImagesNamesList[imManager->testingIndexes[index+shift]]);
        VECTOR1 desiredoutput = helperfunctions::flattenVector(imManager->boundaryBoxesAllImages[imManager->testingIndexes[index+shift]][0]);
        //output is the same as as.back()
        VECTOR1 output = feedForwardWholeNN(imageMatrix);
        helperfunctions::print("Output: ");
        helperfunctions::printVector(output);
        std::cout << std::endl << "desiredOutput: " << std::endl;
        helperfunctions::printVector(desiredoutput);
        helperfunctions::print("Losses: ");
        for(int index = 0; index < desiredoutput.size(); index++){
            helperfunctions::print(Cnn::cos(output[index], desiredoutput[index]));
        }
        if(show){
            std::string imgName = imManager->ImagesNamesList[imManager->testingIndexes[index+shift]];
            showImage(output, desiredoutput ,imgName.substr(0, imgName.size()-3)+"jpg");
        }
    }
}

void Cnn::showImage(VECTOR1 output, VECTOR1 desired ,std::string imageFileName){
    std::ofstream outtxt;
    outtxt.open("output.txt");
    outtxt << imageFileName << std::endl;
    for (auto x : output){
        outtxt << x << std::endl;
    }
    for (auto x : desired){
        outtxt << x << std::endl;
    }
    
    system("python3 ./imageWithOutput.py");
}

double long Cnn::cos(double long output, double long desiredoutput){
    return pow((desiredoutput - output), 2);
}
double long Cnn::DER_cos(double long output, double long desiredoutput){
    return 2*(output - desiredoutput);
}

double long Cnn::leaky_RELU(double long number){ 
    return std::max(0.1*number, number);
    //return std::max( (long double)0, number);
}
double long Cnn::DER_leaky_RELU(double long number){
    return (number >= 0) ? 1 : 0.1;
    //return (number >=0 ) ? 1 : 0;
}

double long Cnn::filter_Activation(double long number){
    return 2/(1+exp(-2*number)) -1;
}
double long Cnn::DER_filter_Activation(double long number){
    return 1-pow(filter_Activation(number), (double long)2);
}

Cnn::~Cnn(){
    delete(imManager);
}