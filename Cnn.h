#ifndef __CNN__
#define __CNN__
#include "defines.h"
#include "imageManagement.h"

class Cnn{
private:

    /*
    CNN stuff
    */


    VECTOR4 processingImagesMaxPooled;
    VECTOR4 processingImagesNoActivationFunction;

    //is either 0 or 1 => if index is used
    VECTOR4 maxPoolingMap;

    VECTOR2 useFilter(VECTOR2 image, VECTOR2 filter, VECTOR2* imgNoActivationFunction);
    VECTOR2 maxPool(VECTOR2 image, int sizeX, int sizeY, VECTOR2* imagePoolingMap);
    // filterstruct = {{{yil, xil}, {yil, xil}...}, {{yil, xil}{yil, xil}}, layer}
    VECTOR4 createRandomFilters(INTVECTOR3 filterstruct);
    VECTOR3 feedForwardFilters(VECTOR2 image);

    void sgdFilters(VECTOR3 cosWRa, VECTOR4* filterUpdates);


    VECTOR2 maxPoolCosWRaMapping(VECTOR2 maxPoolmap, int layer, VECTOR2 cosWRa);
    /*
    dense layer stuff
    */

    VECTOR2 as;
    VECTOR2 zs;

    //used Activationfunction
    double long leaky_RELU(double long number);
    double long DER_leaky_RELU(double long number);

    //initializing weights for dense Layers with random Values 
    VECTOR3 createRandomWeights();
    //initializing biases for dense Layers with random Values
    VECTOR2 createRandomBiases();
    //initializing Zs and AS with zero
    VECTOR2 createZeroZsAndAs();

    //feed forward dense layer neural network
    VECTOR1 feedForwardDNN(VECTOR1 input);

    //backpropagate dense layer neural network => stochastic gradient descent NN => biasesUpdate and weightsUpdate => empty VECTORS as pointers returns las loss with respect to a
    VECTOR1 sgdDNN(VECTOR1 desiredOutput, VECTOR2* biasesUpdate, VECTOR3* weightsUpdate);

    //activationFunction filters
    double long filter_Activation(double long nmbr);
    double long DER_filter_Activation(double long nmbr);

    /*
    combined network stuff
    */
    VECTOR1 feedForwardWholeNN(VECTOR2 imageMatrix);
    void optimize(VECTOR3 weightsUpdate, VECTOR2 biasesUpdate, VECTOR4 filterUpdate);
    
    void showImage(VECTOR1 output, VECTOR1 desiredOutput ,std::string imageFilename);

    //lostFunction
    double long cos(double long output, double long desiredoutput);
    double long DER_cos(double long output, double long desiredoutput);

    //optimizer
    //0.000000000004
    double long lrRate = 0.0000000000003453;

    //momentum
    void initMomentum();
    double long momentumGamma = 0.8;
    void sgdMomentum(double long *velocity, double long *toUpdate ,double long update);

    //SGD
    void stochasticGradientDescent(double long *toUpdate, double long update);


public:
    imageManagement* imManager;

    //filters --> [Layer][filter]
    VECTOR4 filters;

    //filtersSize -> [layer][filter][0] => sizeX [layer][filter][1] => sizeY
    INTVECTOR3 filtersSize;

    //maxPools --> SizeX = [Layer][0] SizeY = [Layer][1] | [Layer].size()=0 
    VECTOR2 maxPools;
    

    //defines denslayerNetworkSize -> {inputSize, amountofNeurons, amountofNeurons, ..., outputSize}
    INTVECTOR1 NNsize;
    //usage: weights[Layer without CL][Lneuron][Rneuron];
    VECTOR3 weights;
    //usage: biases[Layer without CL and wout inputLayer][Lneuron];
    VECTOR2 biases;

    //momenutum
    VECTOR2 velocitysbiases;
    VECTOR3 velocitysweights;
    VECTOR4 velocitysfilters;

    Cnn();
    Cnn(INTVECTOR1 NNsizeP, INTVECTOR3 filtersSize, VECTOR2 maxPoolsP);
    Cnn(INTVECTOR1 NNsizeP, VECTOR2 biasesP, VECTOR3 weightsP, VECTOR4 filtersP ,INTVECTOR3 filtersSizeP, VECTOR2 maxPoolsP);
    ~Cnn();
    void train(int shift, int count, bool show);
    VECTOR1 predictSet(int shift , int count, bool show);
};

#endif
