#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include "helperfunctions.h"




//form => [[[a]], [[b],[c]]] => [[a]], [[b],[c]] 
std::vector<std::string> helperfunctions::splitStringToVector(std::string stringtosplit){
    std::vector<std::string> stringvector;
    //std::cout << stringtosplit << std::endl;
    //remove first and last char
    stringtosplit = stringtosplit.substr(1, stringtosplit.size() - 2);

    int vectorshift = 0;
    int stringshift = 0;
    for(int x = 0; x<stringtosplit.size(); x++){
        if(x==(stringtosplit.size()-1)){
            stringvector.push_back(stringtosplit.substr(stringshift,x-stringshift+1));
        } 
        else if(stringtosplit[x] == '[') vectorshift++;
        else if(stringtosplit[x]==']') vectorshift--;
        else if((stringtosplit[x]==',' && vectorshift==0)){
                stringvector.push_back(stringtosplit.substr(stringshift,x-stringshift));
                stringshift = x+1;
        
                
        }
    }
    if (stringvector.size()==0) stringvector.push_back(stringtosplit);
    return stringvector;
}

std::string helperfunctions::removeSpaces(std::string string){
    string.erase(std::remove(string.begin(), string.end(), ' '), string.end());
    return string;
}

//form "23", "42", "3452"
std::vector<long double> helperfunctions::StringVectorToNumberVector(std::vector<std::string> stringVector){
    std::vector<long double> numberVector;
    for(int x = 0; x<stringVector.size(); x++){
        numberVector.push_back(std::stold(stringVector[x]));
    }
    return numberVector;
}

//form => [[[][]], [], []]]
std::vector<std::vector<std::vector<double long>>> helperfunctions::DataFromPerLineString(std::string line){
    if(line == "[]" || line == "[0]") return {{{0}}};
    std::vector<std::vector<std::vector<double long>>> DataforOneline;
    std::vector<std::string> step1 = splitStringToVector(line);
    for(int x = 0; x<step1.size(); x++){
        std::vector<std::string> step2 = splitStringToVector(step1[x]);
        std::vector<std::vector<double long>> d2vector;
        for(int y = 0; y<step2.size(); y++){
            std::vector<std::string> step3 = splitStringToVector(step2[y]);
            std::vector<double long> d1vector = StringVectorToNumberVector(step3);
            d2vector.push_back(d1vector);
        }
        DataforOneline.push_back(d2vector);
    }
    return DataforOneline;
}

double long helperfunctions::sumVector(std::vector<double long> vec){
    double long sum = 0;
    for(double long n : vec){
        sum += n;
    }
    return sum;
}