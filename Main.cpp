#include <iostream>
#include <fstream>

#include "defines.h"
#include "helperfunctions.h"
#include "imageManagement.h"
#include "Cnn.h"
#include "saveNetwork.h"

int main(){
    srand(time(NULL));
    /*VECTOR2 m = imManager->loadOneImage(imManager->ImagesNamesList[imManager->goodImagesIndexes[1]]);
    helperfunctions::printVector(m);*/
    
    saveNetwork* sNN = new saveNetwork("../../../savedNets/testV12.txt");

    INTVECTOR3 filtersSize = {INTVECTOR2(3, {3,3}), INTVECTOR2(3, {7,7}), INTVECTOR2(1, {3, 3}), INTVECTOR2(2, {6, 7})};
    INTVECTOR1 NNsize = {120960, 200, 4};
    VECTOR2 maxPools=VECTOR2(filtersSize.size(), VECTOR1());
    /*maxPools[0] = {3, 3};
    maxPools[2] = {2, 2};
    */
    maxPools[3] = {2,2};
    
    /*VECTOR2 biases;
    VECTOR3 weights;
    VECTOR4 filters;

    helperfunctions::print("readingNetworkProps");
    sNN->readWholeNetwork(&NNsize, &biases, &weights, &filters, &maxPools, &filtersSize);
    Cnn* cnn = new Cnn(NNsize, biases, weights, filters, filtersSize, maxPools);
    */

    Cnn* cnn = new Cnn(NNsize, filtersSize, maxPools);
    cnn->imManager->shuffleImageIndex();

    //sNN->changeFile("./savedNets/testV6.txt");

    int count = 0;
    while (true){
        cnn->train(count * 300 ,300, false);
        helperfunctions::print("savingNetwork");
        std::cout << "Epoch: " << count <<std::endl;
        sNN->saveWholeNetworkVelocitys(cnn->NNsize, cnn->biases, cnn->weights, cnn->filters, cnn->maxPools, cnn->filtersSize ,cnn->velocitysbiases, cnn->velocitysweights, cnn->velocitysfilters);
        if (++count == 20){
            count = 0;
            cnn->imManager->shuffleImageIndex();
        }
    }

    cnn->predictSet(0, 300, true);
    
    

}