import matplotlib.pyplot as mat
from matplotlib.pyplot import ion
import matplotlib.image as matim
import ast
import random
import time


#create Bounding Boxes
boundaryboxes = []
with open("./output.txt") as fp:
    for i, line in enumerate(fp):
        boundaryboxes.append(str(line).replace("\n", ""))

imgpath = "../../imagesNN/"+boundaryboxes[0]
img = matim.imread(imgpath)
imgplot = mat.imshow(img, cmap='gray')

(x1, y1), (x2, y2) = (float(boundaryboxes[1]), float(boundaryboxes[2])), (float(boundaryboxes[3]), float(boundaryboxes[4]))
mat.plot([x1, x1, x2, x2, x1], [y1, y2, y2, y1, y1], marker='o', markersize=3, color="blue")

(x1, y1), (x2, y2) = (float(boundaryboxes[5]), float(boundaryboxes[6])), (float(boundaryboxes[7]), float(boundaryboxes[8]))
mat.plot([x1, x1, x2, x2, x1], [y1, y2, y2, y1, y1], marker='o', markersize=3, color="green")

mat.title(boundaryboxes[0])
mat.show()