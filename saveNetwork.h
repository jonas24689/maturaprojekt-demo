#ifndef __SAVENETWORK__
#define __SAVENETWORK__

#include "defines.h"
#include <string>
#include <fstream>

class saveNetwork{
    public:
        saveNetwork(std::string filePath);
        ~saveNetwork();
        void saveWholeNetwork(INTVECTOR1 NNsize, VECTOR2 biases, VECTOR3 weights, VECTOR4 filters, VECTOR2 maxPool, INTVECTOR3 filtersSize);
        void saveWholeNetworkVelocitys(INTVECTOR1 NNsize, VECTOR2 biases, VECTOR3 weights, VECTOR4 filters, VECTOR2 maxPool, INTVECTOR3 filtersSize, VECTOR2 biasesMomentum, VECTOR3 weightsMomentum, VECTOR4 filtersMomentum);
        void readWholeNetwork(INTVECTOR1* NNsize, VECTOR2* biases, VECTOR3* weights, VECTOR4* filters, VECTOR2* maxPool, INTVECTOR3* filtersSize);
        void readConstructorProps(INTVECTOR1* NNsize, INTVECTOR3* filtersSize, VECTOR2* maxPool);
        void changeFile(std::string filePath);

    private:
        int PRECISION = 80;
        std::string file;
        std::fstream networkFile;
        void saveVEC1(VECTOR1 vec1);
        void saveVEC1(INTVECTOR1 vec1);
        void saveVEC2(VECTOR2 vec2);
        void saveVEC2(INTVECTOR2 vec2);
        void saveVEC3(VECTOR3 vec3);
        void saveVEC3(INTVECTOR3 vec3);
        void saveVEC4(VECTOR4 vec4);

        VECTOR1 readVEC1();
        INTVECTOR1 readVEC1Int();
        VECTOR2 readVEC2();
        INTVECTOR2 readVEC2Int();
        VECTOR3 readVEC3();
        INTVECTOR3 readVEC3Int();
        VECTOR4 readVEC4();

        void saveDNN(INTVECTOR1 NNsize, VECTOR2 biases, VECTOR3 weights);
        void saveCNN(VECTOR4 filters, VECTOR2 maxPool, INTVECTOR3 filtersSize);
        
        void saveVelocitysMomentum(VECTOR2 biasesMomentum, VECTOR3 weightsMomentum, VECTOR4 filtersMomentum);

};


#endif