TARGET = build.out

OBJS =  \
 ImageManagement.o \
 Main.o \
 Cnn.o \
 helperfunctions.o \
 saveNetwork.o \
 optimizers.o

buildpath = ./build/

all: ${TARGET}

${TARGET}: ${OBJS}
	for file in $^; do t=$$t" "${buildpath}$$file; done && echo $$t && g++ -Wall -g -o ${buildpath}$@ $$t

%.o: %.cpp
	g++ -c -Wall -g -o ${buildpath}$@ $^
 
clean: 
	rm ${buildpath}* || true

run:
	make clean
	make
	chmod a+x ${buildpath}${TARGET}
	clear
	exec ${buildpath}${TARGET}