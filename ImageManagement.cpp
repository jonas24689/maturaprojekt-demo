#include <fstream>
#include <iostream>
#include <random>
#include <algorithm>

#include "imageManagement.h"
#include "helperfunctions.h"



imageManagement::imageManagement(){
    ImagesNamesList = loadImageList();
    loadGoodImagesIndexes(6000, &trainingIndexes, &testingIndexes);
    boundaryBoxesAllImages = loadBoundaryBoxes();
    std::cout << "Loading finished" << std::endl;
}

imageManagement::~imageManagement(){

}

VECTOR2 imageManagement::loadOneImage(std::string imgName){
    std::fstream input(TEXTIMAGESPATH+imgName);
    std::string TextArray;
    getline(input, TextArray);
    std::vector<std::string> firststring = helperfunctions::splitStringToVector(helperfunctions::removeSpaces(TextArray));
    VECTOR2 picturematrix;
    for(int x = 0; x<firststring.size(); x++) picturematrix.push_back(helperfunctions::StringVectorToNumberVector(helperfunctions::splitStringToVector(firststring[0])));
    if(input.is_open()) input.close();
    return picturematrix;
}

std::vector<std::string> imageManagement::loadImageList(){
    std::fstream input("../../Dataset/imagelist.txt");
    std::vector<std::string> imageNames;
    for(std::string line; getline(input, line);){
        imageNames.push_back(line.substr(0,line.size()-3)+"txt");
    }
    if(input.is_open()) input.close();
    //helperfunctions::printVector(imageNames);
    return imageNames;
}

void imageManagement::loadGoodImagesIndexes(int trainSetSize, INTVECTOR1* trainset, INTVECTOR1* testset){
    std::fstream input("../../Dataset/goodImagesOnePerson.txt");
    INTVECTOR1 goodImagesIndexesTrain;
    INTVECTOR1 goodImagesIndexesTest;
    for(std::string line; getline(input, line);){
        if(goodImagesIndexesTrain.size()<=trainSetSize) goodImagesIndexesTrain.push_back(std::stoi(line));
        else goodImagesIndexesTest.push_back(std::stoi(line));
    }
    if(input.is_open()) input.close();
    *trainset = goodImagesIndexesTrain;
    *testset = goodImagesIndexesTest;
}

VECTOR4 imageManagement::loadBoundaryBoxes(){
    std::fstream input("../../Dataset/newData/boundaryboxes.txt");
    std::vector<std::string> boundaryBoxesperline;
    for(std::string line; getline(input, line);) boundaryBoxesperline.push_back(helperfunctions::removeSpaces(line));
    //helperfunctions::printVector(boundaryBoxesperline);
    VECTOR4 boundaryBoxesForAllImages;
    for(int x = 0; x<boundaryBoxesperline.size()-1; x++) boundaryBoxesForAllImages.push_back(helperfunctions::DataFromPerLineString(boundaryBoxesperline[x]));
    if(input.is_open()) input.close();
    //helperfunctions::printVector(boundaryBoxesForAllImages);
    //std::cout << boundaryBoxesForAllImages.size() << std::endl;
    return boundaryBoxesForAllImages;
}

void imageManagement::shuffleImageIndex(){
    std::random_shuffle(std::begin(trainingIndexes), std::end(trainingIndexes));
    std::random_shuffle(std::begin(testingIndexes), std::end(testingIndexes));
}